#!/bin/bash

source .env

export DCMAPIKEY

action=$1
args=$2

# to csv:
#bash $0 getDeliveredQuotes | jq -r '(.result.opportunity[0] | keys_unsorted), (.result.opportunity[] | to_entries | map(.value)) | @csv' | head
dcm=""
if [ -f /usr/local/bin/dcm ]; then
       dcm=/usr/local/bin/dcm
fi
if [ -f /home/jdat/bin/dcm ]; then
	dcm=/home/jdat/bin/dcm
fi

if [ "$action" = "help" ]; then
	echo "USAGE $0 [action]:
		actions:
		help - this message
		getDealStages
		getDeliveredQuotes
		getAllAccounts
		getAllLeads
		getAccounts
		getFields
		getLead
		getLeads
		getOpps - download opportunities
		genUpdate - update using Opps downloaded	
		pullInvoiceList
		uploadAccountsUpdate - uploads accountUpdateFile.tsv (first column is id)
		uploadOppsUpdate - upload oppsUpdateFile.tsv (first column is id)
		uploadLeadsUpdate - upload leadsUpdateFile.tsv (first column is id)
		updateLeads - getOpps then genUpdate
		hourlyCycle - getAllLeads, getAllAccounts, pullInvoiceList, updateLeads, updateAcctsOpenBalance
		updateAcctsOpenBalance - update Accounts open Balance, total invoices, etc.
		updateJdat - update all the jdat variables


	NOTE: Sharpspring Schema:
		Opportunity has a PrimaryLeadId
		Leads & Opportunities have an AccountId
		"
exit 0
fi

if [ ! -d tmp ]; then
	mkdir tmp
fi

if [[ -z "$accountID"  ||  -z "$secretKey" ]]; then
	echo "No Sharpspring key found"
	exit 1
fi

if [[ -z "$dcm"  ||  -z "$DCMAPIKEY" ]]; then
	echo "No dcm found"
	exit 1
fi

if [ "$action" = "updateJdat" ]; then
	bash $0 getAllLeads
	bash $0 getAllAccounts
	bash $0 getOpps
	bash $0 pullInvoiceList

fi

if [ "$action" = "hourlyCycle" ]; then
	bash $0 getAllLeads
	bash $0 getAllAccounts
	bash $0 pullInvoiceList
	bash $0 updateLeads
	bash $0 updateAcctsOpenBalance
fi

if [ "$action" = "updateLeads" ]; then
	bash $0 getOpps
	bash $0 genUpdate
	exit 0
fi

if [ "$action" = "getFields" ]; then
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d '{
			"id":"TEST1",
			"method":"getFields",
			"params":{"where":{},"limit":500,"offset":0}
		}' \
		-H 'Content-Type: application/json' \
		-X 'POST'

	echo

exit 0
fi


if [ "$action" = "getLead" ]; then
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d '{
			"id":"TEST1",
			"method":"getLead",
			"params":{"id":"776529198083"}
		}' \
		-H 'Content-Type: application/json' \
		-X 'POST'

	echo

exit 0
fi

if [ "$action" = "getAllLeads" ]; then
	bash $0 getLeads 0
	bash $0 getLeads 500
	bash $0 getLeads 1000
	cat tmp/getLeads.*.tsv > tmp/getAllLeads.tsv
	$dcm -a put -k proeng_getAllLeads.tsv -f tmp/getAllLeads.tsv

	exit 0
fi

if [ "$action" = "getAllAccounts" ]; then
	bash $0 getAccounts 0
	bash $0 getAccounts 500
	bash $0 getAccounts 1000
	cat tmp/getAccounts.*.tsv > tmp/getAllAccounts.tsv
	$dcm -a put -k proeng_getAllAccounts.tsv -f tmp/getAllAccounts.tsv

	exit 0
fi


if [ "$action" = "pullInvoiceList" ]; then
	curl -L "$INVOICE_LIST_URL" \
		| grep -v '^[[:space:]]' \
		> tmp/pullInvoiceList.tsv
	$dcm -a put -k proeng_pullInvoiceList.tsv -f tmp/pullInvoiceList.tsv
	curl -L "$LEAD_NAME_LOOKUP_URL" \
		> tmp/lead_name_lookup.tsv
	$dcm -a put -k proeng_lead_name_lookup.tsv -f tmp/lead_name_lookup.tsv

fi

if [ "$action" = "getLeads" ]; then
	offset=0
	if [ ! -z "$args" ]; then
		offset=$args
	fi
	data='{
			"id":"TEST1",
			"method":"getLeads",
			"params":{
				"where":[],
				"limit":500,
				"offset":'$offset'
				}
		}'
	echo $data

	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| tee tmp/getLeads.$offset.json \
			| jq -r '(.result.lead[0] | keys_unsorted), (.result.lead[] | to_entries | map(.value)) | @tsv' \
			> tmp/getLeads.$offset.tsv

	echo

exit 0
fi

if [ "$action" = "getAccounts" ]; then
	offset=0
	if [ ! -z "$args" ]; then
		offset=$args
	fi
	data='{
			"id":"TEST1",
			"method":"getAccounts",
			"params":{
				"where":[],
				"limit":500,
				"offset":'$offset'
				}
		}'
	echo $data

	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| tee tmp/getAccounts.$offset.json \
			| jq -r '(.result.account[0] | keys_unsorted), (.result.account[] | to_entries | map(.value)) | @tsv' \
			> tmp/getAccounts.$offset.tsv

	echo

exit 0
fi

if [ "$action" = "getClients" ]; then
	offset=0
	if [ ! -z "$args" ]; then
		offset=$args
	fi
	data='{
			"id":"TEST1",
			"method":"getClients",
			"params":{
				"where":[],
				"limit":500,
				"offset":'$offset'
				}
		}'
	echo $data

	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| jq .

fi

if [ "$action" = "uploadOppsUpdate" ]; then

	if [ ! -f oppsUploadFile.tsv ]; then
		echo "ERROR: oppsUploadFile.tsv must exist!"
		exit 1
	fi

	php csvtoupdateobject.php oppsUploadFile.tsv > oppsUploadFile.json

	tail -n +2 oppsUploadFile.tsv | cut -f1  > oppsUploadIDList.csv

	for n in `cat oppsUploadIDList.csv`; do
	echo $n
		result=$(jq -r '."'$n'"' oppsUploadFile.json )
		echo $result 
		data='{
			"id":"TEST1",
			"method":"updateOpportunities",
			"params": {
				"objects":[
					'$result'
				]
			}
		}'
		echo $data
		 curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
			-d "$data" \
			-H 'Content-Type: application/json' \
			-X 'POST'
	done
	exit 0
fi

if [ "$action" = "uploadAccountsUpdate" ]; then

	if [ ! -f accountUploadFile.tsv ]; then
		echo "ERROR: accountUploadFile.tsv must exist!"
		exit 1
	fi

	php csvtoupdateobject.php accountUploadFile.tsv > accountUploadFile.json

	tail -n +2 accountUploadFile.tsv | cut -f1  > accountUploadIDList.csv

	for n in `cat accountUploadIDList.csv`; do
	echo $n
		result=$(jq -r '."'$n'"' accountUploadFile.json )
		echo $result 
		data='{
			"id":"TEST1",
			"method":"updateAccounts",
			"params": {
				"objects":[
					'$result'
				]
			}
		}'
		echo $data
		curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
			-d "$data" \
			-H 'Content-Type: application/json' \
			-X 'POST'
	done
	exit 0
fi

if [ "$action" = "uploadLeadsUpdate" ]; then

	if [ ! -f leadsUploadFile.tsv ]; then
		echo "ERROR: leadsUploadFile.tsv must exist!"
		exit 1
	fi

	php csvtoupdateobject.php leadsUploadFile.tsv > leadsUploadFile.json

	tail -n +2 leadsUploadFile.tsv | cut -f1  > leadsUploadIDList.csv

	for n in `cat leadsUploadIDList.csv`; do
	echo $n
		result=$(jq -r '."'$n'"' leadsUploadFile.json )
		echo $result 
		data='{
			"id":"TEST1",
			"method":"updateLeads",
			"params": {
				"objects":[
					'$result'
				]
			}
		}'
		echo $data
		 curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
			-d "$data" \
			-H 'Content-Type: application/json' \
			-X 'POST'
	done
	exit 0
fi

if [ "$action" = "updateLeads" ]; then
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d '{
			"id":"TEST1",
			"method":"updateLeads",
			"params": {
				"objects":[{
					"id":"776529198083",
					"pendingquotes_6202ec13d7762":"test"
				}]
			}
		}' \
		-H 'Content-Type: application/json' \
		-X 'POST'

	echo

exit 0
fi

if [ "$action" = "genUpdate" ]; then
	jq -rs '.[] | .primaryLeadID  ' testAllOpps.json | sort | uniq > testOppUpd.csv
	for n in `cat testOppUpd.csv`; do
		echo $n 
		result=$(jq -rs '.[] | select(.primaryLeadID=="'$n'") | select(.dealStageID=="508610563") | .opportunityName' testOpp.json | \
			sed -r ':a;N;$!ba;s/\n/\\n/g' )
		echo $result 
		data='{
			"id":"TEST1",
			"method":"updateLeads",
			"params": {
				"objects":[{
					"id":"'$n'",
					"pendingquotes_6202ec13d7762":"'$result'"
				}]
			}
		}'
		echo $data
		curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
			-d "$data" \
			-H 'Content-Type: application/json' \
			-X 'POST'
	done
	exit 0
fi
if [ "$action" = "genUpdate2" ]; then

	if [ ! -f testOppUpdInner.json ]; then
		echo "run genUpdateInner first: missing testOppUndInner.json file"
		exit 1
	fi
	data='{
		"id":"TEST1",
		"method":"updateLeads",
		"params": {
			"objects":'$(jq -rs '.' testOppUpdInner.json)'
		}
	}'
	echo $data
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST'
	exit 0
fi
if [ "$action" = "genUpdateInner" ]; then
	jq -rs '.[] | .primaryLeadID  ' testAllOpps.json | sort | uniq > testOppUpd.csv
	echo -n > testOppUpdInner.json
	for n in `cat testOppUpd.csv`; do
		echo $n 
		result=$(jq -rs '.[] | select(.primaryLeadID=="'$n'") | select(.dealStageID=="508610563") | .opportunityName' testOpp.json | \
			sed -r ':a;N;$!ba;s/\n/\\n/g' )
		echo $result 
		data=' {
			"id":"'$n'",
			"pendingquotes_6202ec13d7762":"'$result'"
		}'
		echo $data >> testOppUpdInner.json 
	done
	exit 0
fi

if [ "$action" = "getDealStages" ]; then
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d '{
			"id":"TEST1",
			"method":"getDealStages",
			"params":{"where":{},"limit":500,"offset":0}
		}' \
		-H 'Content-Type: application/json' \
		-X 'POST'

	echo

	exit 0
fi


if [ "$action" = "getOpps" ]; then
	nowdate=`date '+%Y-%m-%d %H:%M:%S'`
	data='{
			"id":"TEST2",
			"method":"getOpportunitiesDateRange",
			"params":{
				"startDate":"2021-07-01 00:00:00",
				"endDate":"'$nowdate'",
				"timestamp":"update",
				"limit":5000,
				"offset":0
			}
		}'
	 	echo "$data"
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| jq -r '.result.opportunity[] ' \
				> testAllOpps.json
	cat testAllOpps.json | jq -rs '.[] | select(.isClosed=="0") | select(.isActive=="1") ' \
		> testOpp.json
	
	cat testOpp.json | jq -rs '.[] | select(.dealStageID=="508610563") ' | jq -rs ' (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
		> testOpp_quotes_delivered.csv

	cat testOpp.json | jq -rs '.[] | select(.dealStageID=="508609539") ' | jq -rs ' (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
		> testOpp_quotes_dev.csv

	cat testOpp.json | jq -rs '.[] | select(.dealStageID=="514338819") ' | jq -rs ' (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
		> testOpp_quotes_rev.csv

	cat testOpp.json | jq -rs '.[] | select(.dealStageID=="508608515") ' | jq -rs ' (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
		> testOpp_sql.csv

	cat testAllOpps.json | jq -rs '.[] ' | jq -rs ' (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
		> testOpp_all.csv

	$dcm -a put -k proeng_quotesdelivered -f testOpp_quotes_delivered.csv
	$dcm -a put -k proeng_quotes_dev -f testOpp_quotes_dev.csv
	$dcm -a put -k proeng_quotes_rev -f testOpp_quotes_rev.csv
	$dcm -a put -k proeng_opp_sql -f testOpp_sql.csv
	$dcm -a put -k proeng_opp_all -f testOpp_all.csv

	#$dcm -a put -k "proeng_quotesdelivered:count" -v $(( "`cat testOpp_quotes_delivered.csv | wc -l `" - 1))


	#echo $counter
	exit 0
fi
#pendingquotes_6202ec13d7762



if [ "$action" = "getDeliveredQuotes2" ]; then

	nowdate=`date '+%Y-%m-%d %H:%M:%S'`
	data='{
			"id":"TEST2",
			"method":"getOpportunitiesDateRange",
			"params":{
				"startDate":"2021-07-01 00:00:00",
				"endDate":"'$nowdate'",
				"timestamp":"update",
				"limit":1000,
				"offset":0
			}
		}'
	 	echo "$data"
	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d "$data" \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| jq -r '.result.opportunity[] | select(.isClosed=="0") | select(.dealStageID=="508610563")' \
				> testOpp.json
#			| jq -r '.result.opportunity[] | length'
			cat testOpp.json | jq -rs '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
				> testOpp.csv

	echo
	exit 0

fi

if [ "$action" = "getDeliveredQuotes" ]; then

	curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
		-d '{
			"id":"TEST1",
			"method":"getOpportunities",
			"params":{"where":{"dealStageID":"508610563"},"limit":50000,"offset":0}
		}' \
		-H 'Content-Type: application/json' \
		-X 'POST' \
			| jq -r '.result.opportunity[] | length'
#			| jq -r '.result.opportunity[] | select(.isClosed=="0")' \
#				> testOpp.json
#			cat testOpp.json | jq -rs '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
#				> testOpp.csv

	echo
	exit 0

fi

if [ "$action" = "updateAcctsOpenBalance" ]; then

	curl "$ACCOUNTS_OPEN_BALANCE_API_URL" \
		> tmp/oppsOpenBalance.json

	$dcm -a put -k oppsOpenBalance.json -f tmp/oppsOpenBalance.json

	cat tmp/getAccounts.*.json | \
		jq -sr '.[0].result.account,.[1].result.account,.[2].result.account | .[] | {id: .id, qbid: .quickbooks_customer_number_6245e53fc6f5f} | select(.qbid != null) | .qbid' \
			> tmp/uniqAccountsWithQbids.csv

	successcheck=$(cat tmp/oppsOpenBalance.json | jq '.invqbids | length' )

	if [ "$successcheck" = "0" ]; then
		echo "ERROR $0 updateAcctsOpenBalance tmp/oppsOpenBalance.json download failure"
		echo "check: $successcheck"
		exit 1
	fi

# foreach unique qbid in master opps file
# update corresponding opp with 
# - list of open invoices - 4712199171
# - open balance - 4712198147 



	for qbid in `cat tmp/uniqAccountsWithQbids.csv`; do
		#echo $qbid

		OpenStr=$(cat tmp/oppsOpenBalance.json | jq -r ".invqbids.\"$qbid\"._openstr" )
		OpenTotal=$(cat tmp/oppsOpenBalance.json | jq -r ".invqbids.\"$qbid\"._opentotal" )
		InvCnt=$(cat tmp/oppsOpenBalance.json | jq -r ".invqbids.\"$qbid\"._cnt" )
		SSAcctId=$(cat tmp/oppsOpenBalance.json | jq -r ".invqbids.\"$qbid\"._SSAcctId" )
		#echo $SSAcctId,$InvCnt,$OpenTotal,$OpenStr

		result=$(cat tmp/oppsOpenBalance.json | jq -r ".invqbids.\"$qbid\" | {id: ._SSAcctId , open_balance_625458c1e767e: ._opentotal , open_invoices_625458e610642: ._openstr , open_invoice_count_62545b054bb2e: ._cnt } " )

		#result=$(jq -r '."'$n'"' accountUploadFile.json )
		#echo $result 
		data='{
			"id":"TEST1",
			"method":"updateAccounts",
			"params": {
				"objects":[
					'$result'
				]
			}
		}'
		echo $data
		if [ "$SSAcctId" != "null" ]; then
			curl "https://api.sharpspring.com/pubapi/v1?accountID=${accountID}&secretKey=${secretKey}" \
				-d "$data" \
				-H 'Content-Type: application/json' \
				-X 'POST'
		fi
	done



exit 0
fi

